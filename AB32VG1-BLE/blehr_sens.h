/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef H_BLEHR_SENSOR_
#define H_BLEHR_SENSOR_

#include "nimble/ble.h"
#include "modlog/modlog.h"
#include "console/console.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Heart-rate configuration */
#define GATT_HRS_UUID                           0x180D
#define GATT_HRS_MEASUREMENT_UUID               0x2A37//小程序端
#define GATT_HRS_BODY_SENSOR_LOC_UUID           0x2A38//小程序端
#define GATT_DEVICE_INFO_UUID                   0x180A
#define GATT_MANUFACTURER_NAME_UUID             0x2A29
#define GATT_MODEL_NUMBER_UUID                  0x2A24
#define GATT_STRING_UUID                        0x2A3D //new add at12.2
#define GATT_LEFT_UUID                          0X5A4D//new
#define GATT_RIGHT_UUID                         0X5A5D//new
#define GATT_WY_TEST_UUID                       0x181F//add at 128
#define GATT_Battery_Level_UUID                 0x2A19//电池信息
extern uint16_t hrs_hrm_handle;
extern uint16_t my_notify_handle;   //my notify 129

int gatt_svr_init(void);

#ifdef __cplusplus
}
#endif

#endif
