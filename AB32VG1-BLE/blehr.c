#include <assert.h>
#include <string.h>
#include <stdio.h>

#include <rtthread.h>

#include "nimble/ble.h"
#include "host/ble_hs.h"
#include "services/gap/ble_svc_gap.h"
#include "blehr_sens.h"

static bool notify_state;
static uint16_t notify_conn_handle;//要通过其执行过程的连接。
static uint16_t my_notify_conn_handle;//add 129

static const char *device_name = "GAMEPAD";

static int blehr_gap_event(struct ble_gap_event *event, void *arg);

static uint8_t blehr_addr_type;

/* Sending notify data timer *///发送通知数据计时器
static struct ble_npl_callout blehr_tx_timer;
static struct ble_npl_callout my_notify_tx_timer;

/* Variable to simulate heart beats */// 模拟心跳的变量
static uint8_t heartrate = 90;
static uint8_t XXX=15;
/*
 * Enables advertising with parameters:
 *     o General discoverable mode
 *     o Undirected connectable mode
 * 启用具有以下参数的广告：
 * o 一般可发现模式
 * o 无向可连接模式

 */
static void blehr_advertise(void)
{
    struct ble_gap_adv_params adv_params;
    struct ble_hs_adv_fields fields;
    int rc;

    /*
     *  Set the advertisement data included in our advertisements:
     *     o Flags (indicates advertisement type and other general info)
     *     o Advertising tx power
     *     o Device name
     * 设置广告中包含的广告数据：
     * o 标志（指示广告类型和其他一般信息）
     * o 广告技术力量
     * o 设备名称
     */
    memset(&fields, 0, sizeof(fields));

    /*
     * Advertise two flags:
     *      o Discoverability in forthcoming advertisement (general)
     *      o BLE-only (BR/EDR unsupported)
     * * 宣传两个标志：
     * o 即将发布的广告中的可发现性（一般）
     * o 仅 BLE（不支持 BR/EDR）
     */
    fields.flags = BLE_HS_ADV_F_DISC_GEN |
                    BLE_HS_ADV_F_BREDR_UNSUP;

    /*
     * Indicate that the TX power level field should be included; have the
     * stack fill this value automatically.  This is done by assigning the
     * special value BLE_HS_ADV_TX_PWR_LVL_AUTO.
     * 指示应包括 TX 功率级别字段;拥有
     * 堆栈自动填充此值。 这是通过分配
     * 特殊值BLE_HS_ADV_TX_PWR_LVL_AUTO。
     */
    fields.tx_pwr_lvl_is_present = 1;
    fields.tx_pwr_lvl = BLE_HS_ADV_TX_PWR_LVL_AUTO;

    fields.name = (uint8_t *)device_name;
    fields.name_len = strlen(device_name);
    fields.name_is_complete = 1;

    rc = ble_gap_adv_set_fields(&fields);
    if (rc != 0) {
        MODLOG_DFLT(ERROR, "error setting advertisement data; rc=%d\n", rc);
        return;
    }

    /* Begin advertising *///开始投放广告
    memset(&adv_params, 0, sizeof(adv_params));
    adv_params.conn_mode = BLE_GAP_CONN_MODE_UND;
    adv_params.disc_mode = BLE_GAP_DISC_MODE_GEN;
    rc = ble_gap_adv_start(blehr_addr_type, NULL, BLE_HS_FOREVER,
                           &adv_params, blehr_gap_event, NULL);
    if (rc != 0) {
        MODLOG_DFLT(ERROR, "error enabling advertisement; rc=%d\n", rc);
        return;
    }
}

static void blehr_tx_hrate_stop(void)
{
    ble_npl_callout_stop(&blehr_tx_timer);
}

/* Reset heartrate measurment *///重置心率测量
static void blehr_tx_hrate_reset(void)
{
    int rc;

    rc = ble_npl_callout_reset(&blehr_tx_timer, RT_TICK_PER_SECOND);
    assert(rc == 0);
}

/* This functions simulates heart beat and notifies it to the client */
//此功能模拟心跳并将其通知给客户端
static void blehr_tx_hrate(struct ble_npl_event *ev)
{
    static uint8_t hrm[2];
    int rc;
    struct os_mbuf *om;

    if (!notify_state) {
        blehr_tx_hrate_stop();
        heartrate = 90;
        return;
    }

    hrm[0] = 0x06; /* contact of a sensor *///传感器的联系
    hrm[1] = heartrate; /* storing dummy data *///储存虚拟数据

    /* Simulation of heart beats *///模拟心跳
    heartrate++;
    if (heartrate == 160) {
        heartrate = 90;
    }

    om = ble_hs_mbuf_from_flat(hrm, sizeof(hrm));

    rc = ble_gattc_notify_custom(notify_conn_handle, hrs_hrm_handle, om);

    assert(rc == 0);
    blehr_tx_hrate_reset();
}
//my notify--
static void my_notify_stop(void)
{
    ble_npl_callout_stop(&my_notify_tx_timer);
}

//重置
static void my_notify_reset(void)
{
    int rc;

    rc = ble_npl_callout_reset(&my_notify_tx_timer, RT_TICK_PER_SECOND);
    assert(rc == 0);
}


static void my_notify_tx(struct ble_npl_event* ev2)
{
    static uint8_t my_notify_hrm[2];
    int rc;
    struct os_mbuf *om;

    if (!notify_state) {
        my_notify_stop();
        XXX = 15;
        return;
    }

    my_notify_hrm[0] = 0x02; /* contact of a sensor *///传感器的联系
    my_notify_hrm[1] = XXX; /* storing dummy data *///储存虚拟数据

    /* Simulation of heart beats *///模拟心跳
        XXX++;
    if (XXX == 50) {
        XXX = 90;
    }

    om = ble_hs_mbuf_from_flat(my_notify_hrm, sizeof(my_notify_hrm));

    rc = ble_gattc_notify_custom(my_notify_conn_handle, my_notify_handle, om);

    assert(rc == 0);
    my_notify_reset();
}

static int blehr_gap_event(struct ble_gap_event *event, void *arg)
{
    switch (event->type) {
    case BLE_GAP_EVENT_CONNECT:
        /* A new connection was established or a connection attempt failed */
        //已建立新连接或连接尝试失败
        MODLOG_DFLT(INFO, "connection %s; status=%d\n",
                    event->connect.status == 0 ? "established" : "failed",
                    event->connect.status);

        if (event->connect.status != 0) {
            /* Connection failed; resume advertising */
            //连接失败;恢复广告
            blehr_advertise();
        }
        break;

    case BLE_GAP_EVENT_DISCONNECT:
        MODLOG_DFLT(INFO, "disconnect; reason=%d\n", event->disconnect.reason);

        /* Connection terminated; resume advertising */
        //连接终止;恢复广告
        blehr_advertise();
        break;

    case BLE_GAP_EVENT_ADV_COMPLETE:
        //广播过程完成
        MODLOG_DFLT(INFO, "adv complete\n");
        blehr_advertise();
        break;

    case BLE_GAP_EVENT_SUBSCRIBE:
        //对等方订阅状态的状态更改1?2:3
        MODLOG_DFLT(INFO, "subscribe event; cur_notify=%d\n value handle; "
                          "val_handle1=%d\n","val_handle2=%d\n",
                    event->subscribe.cur_notify, hrs_hrm_handle,my_notify_handle);
        if (event->subscribe.attr_handle !=0) {
            if (event->subscribe.attr_handle==hrs_hrm_handle)
            {
                notify_state = event->subscribe.cur_notify;
                notify_conn_handle = event->subscribe.conn_handle;
                blehr_tx_hrate_reset();
            }
            else if (event->subscribe.attr_handle==my_notify_handle)
            {
                notify_state = event->subscribe.cur_notify;
                my_notify_conn_handle = event->subscribe.conn_handle;
                my_notify_reset();
            }
                        
        } else if (event->subscribe.attr_handle != (hrs_hrm_handle&&my_notify_handle)) {
            notify_state = event->subscribe.cur_notify;
            notify_conn_handle = 0;
            my_notify_conn_handle=0;
            blehr_tx_hrate_stop();
            my_notify_stop();
        }
        break;

    case BLE_GAP_EVENT_MTU:
        MODLOG_DFLT(INFO, "mtu update event; conn_handle=%d mtu=%d\n",
                    event->mtu.conn_handle,
                    event->mtu.value);
        break;

    }

    return 0;
}

static void blehr_on_sync(void)
{
    int rc;

    /* Use privacy */
    //使用秘密
    rc = ble_hs_id_infer_auto(0, &blehr_addr_type);
    assert(rc == 0);

    /* Begin advertising */
    //开始广播
    blehr_advertise();
}

// extern int nimble_ble_enable(void);
extern struct ble_npl_eventq *nimble_port_get_dflt_eventq(void);

int blehr_main(void)
{
    int rc;

    /* Initialize the NimBLE host configuration *///设置回调函数
    ble_hs_cfg.sync_cb = blehr_on_sync;

    //初始化定时器
    ble_npl_callout_init(&blehr_tx_timer, nimble_port_get_dflt_eventq(),
                    blehr_tx_hrate, NULL);
    ble_npl_callout_init(&my_notify_tx_timer,nimble_port_get_dflt_eventq(),
                    my_notify_tx,NULL);
    //初始化gatt服务
    rc = gatt_svr_init();
    assert(rc == 0);

    /* Set the default device name *///设备默认名字
    rc = ble_svc_gap_device_name_set(device_name);
    assert(rc == 0);

    return 0;
}
