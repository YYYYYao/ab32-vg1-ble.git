
#include <assert.h>
#include <stdio.h>
#include <string.h>
#include "host/ble_hs.h"
#include "host/ble_uuid.h"
#include "blehr_sens.h"

static const char *manuf_name = "welcome";//Apache Mynewt
static const char *model_num = "haha";//Mynewt HR Sensor
static const char *change_shape="change";
static const char *turn_left="left";
static const char *turn_right="right";
static const char *usr_Customization1="A1";
static const char *usr_Customization2="B1";

static uint8_t turnLEFT=1;
static uint8_t turnRIGHT=3;
static uint8_t turnCHANGE=2;

static const char *NO_INPUT="ab32vg1";

uint16_t hrs_hrm_handle;
uint16_t my_notify_handle;//my notify
extern uint8_t key;

static int gatt_svr_chr_access_heart_rate(uint16_t conn_handle, uint16_t attr_handle,
                               struct ble_gatt_access_ctxt *ctxt, void *arg);

static int gatt_svr_chr_access_device_info(uint16_t conn_handle, uint16_t attr_handle,
                                struct ble_gatt_access_ctxt *ctxt, void *arg);

static const struct ble_gatt_svc_def gatt_svr_svcs[] = {
    {
        /* Service: Heart-rate */
        .type = BLE_GATT_SVC_TYPE_SECONDARY,
        .uuid = BLE_UUID16_DECLARE(GATT_HRS_UUID),
        .characteristics = (struct ble_gatt_chr_def[]) { {
            /* Characteristic: Heart-rate measurement */
            .uuid = BLE_UUID16_DECLARE(GATT_HRS_MEASUREMENT_UUID),
            .access_cb = gatt_svr_chr_access_heart_rate,
            .val_handle = &hrs_hrm_handle,
            .flags = BLE_GATT_CHR_F_NOTIFY,
        }, {
            /* Characteristic: Body sensor location */
            .uuid = BLE_UUID16_DECLARE(GATT_HRS_BODY_SENSOR_LOC_UUID),
            .access_cb = gatt_svr_chr_access_heart_rate,
            .flags = BLE_GATT_CHR_F_READ,
        }, {
            0, /* No more characteristics in this service */
        }, }
    },

    {
        /* Service: Device Information */
        .type = BLE_GATT_SVC_TYPE_PRIMARY,
        .uuid = BLE_UUID16_DECLARE(GATT_DEVICE_INFO_UUID),
        .characteristics = (struct ble_gatt_chr_def[]) { {
            /* Characteristic: * Manufacturer name */
            .uuid = BLE_UUID16_DECLARE(GATT_MANUFACTURER_NAME_UUID),//*
            .access_cb = gatt_svr_chr_access_device_info,
            .flags = BLE_GATT_CHR_F_READ,
        }, {
            /* Characteristic: Model number string */
            .uuid = BLE_UUID16_DECLARE(GATT_MODEL_NUMBER_UUID),//*
            .access_cb = gatt_svr_chr_access_device_info,
            .flags = BLE_GATT_CHR_F_READ,
        },{
            /*Characteristic:Model number string */
            .uuid=BLE_UUID16_DECLARE(GATT_STRING_UUID),
            .access_cb=gatt_svr_chr_access_device_info,
            .flags=BLE_GATT_CHR_F_READ,
        }, 
  
        {
            /*Characteristic:Model number string */
            .uuid=BLE_UUID16_DECLARE(GATT_RIGHT_UUID),
            .access_cb=gatt_svr_chr_access_device_info,
            .val_handle = &my_notify_handle,
            .flags=BLE_GATT_CHR_F_NOTIFY,
        }, 
        
        {
            0, /* No more characteristics in this service */
        }, }
    },
    {
        //128 service
        .type=BLE_GATT_SVC_TYPE_SECONDARY,
        .uuid=BLE_UUID16_DECLARE(GATT_WY_TEST_UUID),
        .characteristics=(struct ble_gatt_chr_def[]){
        /*    {
                .uuid=BLE_UUID16_DECLARE(GATT_LEFT_UUID),
                .access_cb=gatt_svr_chr_access_device_info,
                .flags=BLE_GATT_CHR_F_READ,
            },*/
            {
                .uuid=BLE_UUID16_DECLARE(GATT_LEFT_UUID),
                .access_cb=gatt_svr_chr_access_device_info,
                .flags=BLE_GATT_CHR_F_READ,
            },
            {
            0
            },
        }
        
    },
/*    {
        .type=BLE_GATT_SVC_TYPE_PRIMARY,
        .uuid=BLE_UUID16_DECLARE(GATT_Battery_Level_UUID),
        .characteristics=(struct ble_gatt_chr_def[]){{
            .uuid=BLE_UUID16_DECLARE(GATT_LEFT_UUID),
            .access_cb=gatt_svr_chr_access_device_info,
            .flags=BLE_GATT_CHR_F_READ,
        }}
    },*/
        {
            0, /* No more services */
        },
};

static int gatt_svr_chr_access_heart_rate(uint16_t conn_handle, uint16_t attr_handle,
                               struct ble_gatt_access_ctxt *ctxt, void *arg)
{
    /* Sensor location, set to "Chest" */
    static uint8_t body_sens_loc = 0x01;
    static char Xtest[]="shark";
    uint16_t uuid;
    int rc;

    uuid = ble_uuid_u16(ctxt->chr->uuid);

    if (uuid == GATT_HRS_BODY_SENSOR_LOC_UUID) {
        rc = os_mbuf_append(ctxt->om, &Xtest, sizeof(Xtest));

        return rc == 0 ? 0 : BLE_ATT_ERR_INSUFFICIENT_RES;
    }

    assert(0);
    return BLE_ATT_ERR_UNLIKELY;
}

static int gatt_svr_chr_access_device_info(uint16_t conn_handle, uint16_t attr_handle,
                                struct ble_gatt_access_ctxt *ctxt, void *arg)
{
    uint16_t uuid;
    int rc;

    uuid = ble_uuid_u16(ctxt->chr->uuid);

    if (uuid == GATT_MODEL_NUMBER_UUID) {

        rc = os_mbuf_append(ctxt->om, model_num, strlen(model_num));
        return rc == 0 ? 0 : BLE_ATT_ERR_INSUFFICIENT_RES;        

    }

    if (uuid == GATT_MANUFACTURER_NAME_UUID) {

        rc=os_mbuf_append(ctxt->om,manuf_name,strlen(manuf_name));
        return rc== 0?0:BLE_ATT_ERR_INSUFFICIENT_RES;

    }
    //new add 
    if (uuid == GATT_STRING_UUID){
        {
                rc=os_mbuf_append(ctxt->om,turn_left,strlen(turn_left));
                return rc== 0?0:BLE_ATT_ERR_INSUFFICIENT_RES;
        }
        if (uuid == GATT_LEFT_UUID){

        rc=os_mbuf_append(ctxt->om,usr_Customization1,strlen(turn_left));
        return rc== 0?0:BLE_ATT_ERR_INSUFFICIENT_RES;

    }
        if (uuid == GATT_RIGHT_UUID){

        rc=os_mbuf_append(ctxt->om,usr_Customization2,strlen(turn_right));
        return rc== 0?0:BLE_ATT_ERR_INSUFFICIENT_RES;

    }
    {
        /* code */
    }
    

    assert(0);
    return BLE_ATT_ERR_UNLIKELY;
}

void gatt_svr_register_cb(struct ble_gatt_register_ctxt *ctxt, void *arg)
{
    char buf[BLE_UUID_STR_LEN];

    switch (ctxt->op) {
    case BLE_GATT_REGISTER_OP_SVC:
        MODLOG_DFLT(DEBUG, "registered service %s with handle=%d\n",
                    ble_uuid_to_str(ctxt->svc.svc_def->uuid, buf),
                    ctxt->svc.handle);
        break;

    case BLE_GATT_REGISTER_OP_CHR:
        MODLOG_DFLT(DEBUG, "registering characteristic %s with "
                           "def_handle=%d val_handle=%d\n",
                    ble_uuid_to_str(ctxt->chr.chr_def->uuid, buf),
                    ctxt->chr.def_handle,
                    ctxt->chr.val_handle);
        break;

    case BLE_GATT_REGISTER_OP_DSC:
        MODLOG_DFLT(DEBUG, "registering descriptor %s with handle=%d\n",
                    ble_uuid_to_str(ctxt->dsc.dsc_def->uuid, buf),
                    ctxt->dsc.handle);
        break;

    default:
        assert(0);
        break;
    }
}

int gatt_svr_init(void)
{
    int rc;

    rc = ble_gatts_count_cfg(gatt_svr_svcs);//调整主机配置对象的设置以适应指定的服务定义数组。
    if (rc != 0) {
        return rc;
    }

    rc = ble_gatts_add_svcs(gatt_svr_svcs);//将一组服务定义排入队列以进行注册。

    if (rc != 0) {
        return rc;
    }

    return 0;
}

