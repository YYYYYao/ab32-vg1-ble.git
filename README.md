# AB32VG1-BLE

#### 介绍
AB32VG1使用BLE和微信小程序通信


使用之前确保对BLE有理解;


软件部分设计逻辑还是十分明确的；

开发板：打开ble，开始广播等待被发现。建立通信后，按照主机端的请求发送相应数据；

小程序：打开蓝牙适配器，搜索周围蓝牙，获取搜索过程中所搜索到的设备信息，连接想要连接的设备，获取服务、特征值，进行数据交互；


贴上代码：

在进行完基本初始化之后，进行对BLE的设置：

    //设置回调函数

    ble_hs_cfg.sync_cb = blehr_on_sync;

    //初始化定时器

    ble_npl_callout_init(&blehr_tx_timer, nimble_port_get_dflt_eventq(),

                    blehr_tx_hrate, NULL);

    ble_npl_callout_init(&my_notify_tx_timer,nimble_port_get_dflt_eventq(),

                    my_notify_tx,NULL);

    //初始化gatt服务

    rc = gatt_svr_init();

    assert(rc == 0);

    //设备默认名字

    //这里可以更改设备名字

    rc = ble_svc_gap_device_name_set(device_name);



我在DEVICE_INFO Service里添加了String(0x2A3D)和0X5A5D 的自定义characteristic；

属性分别为READ和NOTIFY

{

            /*Characteristic:Model number string */

            .uuid=BLE_UUID16_DECLARE(GATT_STRING_UUID),

            .access_cb=gatt_svr_chr_access_device_info,

            .flags=BLE_GATT_CHR_F_READ,

        },

 

        {

            /*Characteristic:Model number string */

            .uuid=BLE_UUID16_DECLARE(GATT_RIGHT_UUID),

            .access_cb=gatt_svr_chr_access_device_info,

            .val_handle = &my_notify_handle,

            .flags=BLE_GATT_CHR_F_NOTIFY,

        },

给2A3D的value为："ab32vg1";